interface ProofModel {
  name: string;
  valid: boolean;
  touched: boolean;
}
interface ProofValidator {
  model: string;
  type: string;
  //type: 'notEmpty' | 'minLength' | 'manual';
  options: any;
  valid: boolean;
}

class Proof {
  private _models: ProofModel[] = [];
  private _validators: ProofValidator[] = [];

  private _changeSubs = [];


  install = (rules) => {
    for (let model in rules) {
      if (rules.hasOwnProperty(model)) {
        this._models.push({name: model, valid: false, touched: false});
        for (let validator in rules[model]) {
          if (rules[model].hasOwnProperty(validator)) {
            this._validators.push({model: model, type: validator, options: rules[model][validator], valid: false});
          }
        }
      }
    }
  };

  validate = (data): boolean => {
    this._validators.forEach((validator: ProofValidator) => {
      this.validateModel(validator.model, data[validator.model]);
    });
    return this.isValid();
  };

  isValid = () => {
    return this._models.every((model: ProofModel) => model.valid);
  };

  validateModel = (model: string, value) => {
    let hasChanges = false;
    this._validators.filter((validator: ProofValidator) => validator.model == model).forEach((validator: ProofValidator) => {
      switch (validator.type) {
        case 'manual':
          break;
        case 'notEmpty':
          if (this.changeValidator(validator, !!value))
            hasChanges = true;
          break;
        case 'minLength':
          if (this.changeValidator(validator, value.length >= validator.options))
            hasChanges = true;
          break;
        case 'email':
          let reg = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
          if (this.changeValidator(validator, reg.test(value)))
            hasChanges = true;
      }
    });
    if (hasChanges) {
      this._updateModelStatus(model);
      this.emitChanges();
    }
  };

  protected changeValidator = (validator: ProofValidator, valid: boolean): boolean => {
    if (validator.valid != valid) {
      validator.valid = valid;
      return true;
    }
    return false;
  };

  private _updateModelStatus = (modelName: string) => {
    let model = this._getModel(modelName);
    let currentStatus = this._validators.filter((validator: ProofValidator) => validator.model == model.name).every((validator: ProofValidator) => validator.valid);
    if (model.valid != currentStatus) {
      model.valid = currentStatus;
    }
    return model.valid;
  };

  private _getModel = (name: string) => {
    return this._models.filter((model: ProofModel) => model.name == name)[0];
  };

  private _getValidator = (modelName: string, validatorType: string) => {
    return this._validators.filter((validator: ProofValidator) => validator.model == modelName && validator.type == validatorType)[0];
  };

  set = (modelName: string, validatorType: string, valid: boolean) => {
    let validator = this._getValidator(modelName, validatorType);
    if (validator.valid != valid) {
      validator.valid = valid;
      this._updateModelStatus(modelName);
      this.emitChanges();
    }
  };

  touch = (modelName: string) => {
    let model = this._getModel(modelName);
    model.touched = true;
  };

  view = (modelName: string, validView, invalidView) => {
    let model = this._getModel(modelName);
    if (model.valid) {
      return validView(model.touched);
    }
    else {
      return invalidView(model.touched);
    }
  };

  subscribeOnChanges = (call) => {
    this._changeSubs.push(call);
  };

  protected emitChanges = () => {
    console.log('emitChanges');
    this._changeSubs.forEach((call) => {
      call();
    });
  }

}